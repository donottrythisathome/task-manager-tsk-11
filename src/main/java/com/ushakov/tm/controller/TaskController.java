package com.ushakov.tm.controller;

import com.ushakov.tm.api.ITaskController;
import com.ushakov.tm.api.ITaskService;
import com.ushakov.tm.model.Task;
import com.ushakov.tm.service.TaskService;
import com.ushakov.tm.util.TerminalUtil;

import java.sql.SQLOutput;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for (final Task task: tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String taskDescription = TerminalUtil.nextLine();
        final Task task = taskService.add(taskName, taskDescription);
        if (task == null) {
            System.out.println("[FAILED] \n");
            return;
        }
        System.out.println("[OK] \n");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        taskService.clear();
        System.out.println("[OK] \n");
    }

    @Override
    public void removeTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(taskId);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(taskName);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
        System.out.println();
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.removeOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY REMOVED \n");
    }

    @Override
    public void findTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void findTaskByName() {
        System.out.println("ENTER TASK NAME");
        final String taskName = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(taskName);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void findTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("NOT FOUND \n");
            return;
        }
        System.out.println(task);
        System.out.println();
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("ENTER TASK INDEX");
        final Integer taskIndex = TerminalUtil.nextNumber();
        if (taskIndex < 1) {
            System.out.println("INCORRECT INDEX \n");
            return;
        }
        final Task task = taskService.findOneByIndex(taskIndex - 1);
        if (task == null) {
            System.out.println("TASK NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskByIndex(taskIndex - 1, taskName, taskDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }

    @Override
    public void updateTaskById() {
        System.out.println("ENTER TASK ID");
        final String taskId = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(taskId);
        if (task == null) {
            System.out.println("TASK NOT FOUND \n");
            return;
        }
        System.out.println("ENTER NAME");
        final String taskName = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION");
        final String taskDescription = TerminalUtil.nextLine();
        if (taskService.updateTaskById(taskId, taskName, taskDescription) == null) {
            System.out.println("FAILED \n");
            return;
        }
        System.out.println("SUCCESSFULLY UPDATED \n");
    }
}
