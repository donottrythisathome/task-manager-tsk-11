package com.ushakov.tm.repository;

import com.ushakov.tm.api.ICommandRepository;
import com.ushakov.tm.constant.ArgumentConst;
import com.ushakov.tm.constant.TerminalConst;
import com.ushakov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    // General Commands

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show application version."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Terminate console application."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show system information."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands."
    );

    // Task Commands

    private static final Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "Show task list."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "Clear task list."
    );

    private static final Command TASK_FIND_BY_ID = new Command(
            TerminalConst.CMD_FIND_TASK_BY_ID, null, "Find task by id."
    );

    private static final Command TASK_FIND_BY_NAME = new Command(
            TerminalConst.CMD_FIND_TASK_BY_NAME, null, "Find task by name."
    );

    private static final Command TASK_FIND_BY_INDEX = new Command(
            TerminalConst.CMD_FIND_TASK_BY_INDEX, null, "Find task by index."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_TASK_BY_INDEX, null, "Remove task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_UPDATE_TASK_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_UPDATE_TASK_BY_INDEX, null, "Update task by index."
    );

    // Project Commands

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "Clear project list."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "Show project list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_FIND_BY_ID = new Command(
            TerminalConst.CMD_FIND_TASK_BY_ID, null, "Find project by id."
    );

    private static final Command PROJECT_FIND_BY_NAME = new Command(
            TerminalConst.CMD_FIND_PROJECT_BY_NAME, null, "Find project by name."
    );

    private static final Command PROJECT_FIND_BY_INDEX = new Command(
            TerminalConst.CMD_FIND_PROJECT_BY_INDEX, null, "Find project by index."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_REMOVE_PROJECT_BY_INDEX, null, "Remove project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_UPDATE_PROJECT_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_UPDATE_PROJECT_BY_INDEX, null, "Update project by index."
    );

    // All commands Array

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, COMMANDS, ARGUMENTS, TASK_LIST, TASK_CREATE, TASK_CLEAR, PROJECT_LIST,
            PROJECT_CREATE, PROJECT_CLEAR, TASK_FIND_BY_ID, TASK_FIND_BY_NAME, TASK_FIND_BY_INDEX,
            TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            PROJECT_FIND_BY_ID, PROJECT_FIND_BY_NAME, PROJECT_FIND_BY_INDEX,
            PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME, PROJECT_FIND_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    };

}
