package com.ushakov.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void removeProjectById();

    void removeProjectByName();

    void removeProjectByIndex();

    void findProjectById();

    void findProjectByName();

    void findProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

}
